extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var enemy_01_path = GlobalVars.enemy_01_path
var hero_path = GlobalVars.hero_path


# Called when the node enters the scene tree for the first time.
func _ready():
	pass

#sets the sprite for the actor
func set_visuals(character:String):
	var char_path
	if character in GlobalVars.character_list:
		char_path = GlobalVars.character_list[character][0]
		var character_scene = load(char_path).instance()
		for child in self.get_children():
			self.remove_child(child)
			child.queue_free()
		add_child(character_scene)
	else: 
		print("Error")
			
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
