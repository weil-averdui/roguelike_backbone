extends Node



var stats = {
	"id":0,
	"current_hp":0,
	"max_hp":0,
	"attack_bonus": 0,
	"damage_bonus": 0,
	"damage_die": 0,
	"crit_rate":0,
	"crit_multiplier": 0,
	"detection_range": 0,
	"defense": 0
}



#sets initial stat array
func set_initial_stats(initial_stats: Dictionary) -> void:
	stats["max_hp"] = initial_stats["max_hp"]
	stats["current_hp"] = initial_stats["max_hp"]
	stats["attack_bonus"] = initial_stats["attack_bonus"]
	stats["damage_bonus"] = initial_stats["damage_bonus"]
	stats["damage_die"] = initial_stats["damage_die"]
	stats["damage_number_dice"] = initial_stats["damage_number_dice"]
	stats["crit_multiplier"] = initial_stats["crit_multiplier"]
	stats["crit_rate"] = initial_stats["crit_rate"]
	stats["defense"] = initial_stats["defense"]
	stats["speed"] = initial_stats["speed"]
	stats["detection_range"] = initial_stats["detection_range"]

func set_id(id: int) -> void:
	stats["id"] = id

func get_id() -> int:
	return stats["id"]
	
func get_range() -> int:
	return stats["detection_range"]

func add_bonuses(bonuses: Dictionary) -> void:
	for stat in bonuses.keys():
		stats[stat]+=bonuses[stat]
		
func get_defense() -> int:
	return stats["defense"]
	
func set_hp(hp:int) -> void:
	stats["current_hp"] = hp

func get_hp() -> int:
	return stats["current_hp"]

func get_speed() -> int:
	return stats["speed"]
		
func get_damage(damage: int) -> int:
	stats["current_hp"] -=damage
	if stats["current_hp"] <=0:
		return(get_id())
	else:
		return -1


func heal(heals: int) -> void:
	stats["current_hp"] = max(stats["current_hp"]+heals, stats["max_hp"])
	
	
#produces a result of attack vs target value	
func do_attack(target_value: int) -> Array:
	var attack_roll = GlobalVars.roll_dice(1,20)
	var attack_value = attack_roll+stats["attack_bonus"]
	if attack_value < target_value:
		if attack_roll == 20:
			var damage = GlobalVars.roll_dice(stats["damage_number_dice"], stats["damage_die"])+stats["damage_bonus"]
			return [damage, "hit"]
		else:
			return[0,"miss"]
	else:
		if attack_roll >= stats["crit_rate"]:
			var confirm_value = GlobalVars.roll_dice(1,20)+stats["attack_bonus"]
			if confirm_value >= target_value:
				var damage = GlobalVars.roll_dice(stats["crit_multiplier"]*stats["damage_number_dice"], stats["damage_die"])+stats["crit_multiplier"]*stats["damage_bonus"]
				return [damage, "crit"]
			else:
				var damage = GlobalVars.roll_dice(stats["damage_number_dice"], stats["damage_die"])+stats["damage_bonus"]
				return [damage, "hit"]
		else:
			var damage = GlobalVars.roll_dice(stats["damage_number_dice"], stats["damage_die"])+stats["damage_bonus"]
			return [damage, "hit"]
	
	

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
