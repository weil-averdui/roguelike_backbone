extends Node2D

onready var map = get_parent()
var char_type = "hero"
var char_group = "friendly"
onready var visuals = get_node("Visuals")
onready var inventory = get_node("Inventory")
onready var fighter = get_node("Fighter")
onready var pathfinder = map.pathfinder
onready var aStar = map.pathfinder.aStar
var current_position: Vector2
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal  hit(old_cell, enemy_cell)

#places an actor on the initial cell
func put_actor(cell) -> void:
	current_position = cell
	set_position(map.map_to_world(cell))

#moves an actor to a new position
func set_map_pos(cell) -> void:
	current_position = cell
	map.state = false
	$Tween.interpolate_property(self,"global_position",self.global_position,map.map_to_world(cell),0.3,Tween.TRANS_QUINT,Tween.EASE_IN_OUT)
	$Tween.start()
	yield($Tween,"tween_all_completed")
	map.state=true

# Get our position in map cell coordinates
func get_map_pos() -> Vector2:
	return map.world_to_map(get_position())

func set_character(type:String) -> void:
	char_type = type
	if type == "hero":
		$Camera.current = true
	fighter.set_initial_stats(GlobalVars.character_list[type][1])
	visuals.set_visuals(type)
	if GlobalVars.character_list[type][2]:
		char_group = "friendly"
	else:
		char_group = "foe"
		inventory.generate_item()
	

#checks if the next cell is free and makes a move (or attack)
func step(dir) -> String:
	dir.x = clamp(dir.x, -1, 1)
	dir.y = clamp(dir.y, -1, 1)
	var old_cell = current_position
	var new_cell = current_position + dir
	var check = map.is_blocked(new_cell)
	if  check== "empty" :
		map.pathfinder.freed(old_cell)
		map.pathfinder.occupy(new_cell)
	elif check=="foe":
		var enemy_cell = new_cell
		new_cell = old_cell
		if char_type == "hero":
			self.emit_signal("hit",old_cell,enemy_cell)
	else:
		new_cell = old_cell
	if char_type == "hero":
		map.hero_position = new_cell
	set_map_pos(new_cell)
	return check

#returns vector between and and start
func get_directions(start:Vector2, end:Vector2) -> Vector2:
	return end-start
	
#uses pathfinding to make a move for an enemy
func do_the_move() -> void:
	match char_group:
		"foe":
			var paths = pathfinder.get_a_path(aStar, current_position, map.hero_position)
			if paths != [] and len(paths)>2 and len(paths)<fighter.get_range():
				for i in range(0,fighter.get_speed()):
					var step_dir = get_directions(current_position, paths[i+1])
					step(step_dir)	
			elif len(paths) == 2:
				self.emit_signal("hit",paths[0],paths[1])
				
# Called when the node enters the scene tree for the first time.
func _ready():
	self.connect("hit",self.get_parent(), "_on_hit")
	$Camera.limit_right = map.a*128+get_viewport_rect().size.x/2
	$Camera.limit_bottom = map.b*128+get_viewport_rect().size.y/2


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
