extends Node

var inventory_list = {"empty":0}
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
func add_item(item: String) -> void:
	if item in inventory_list:
		inventory_list[item] +=1
	else:
		inventory_list[item] = 1
		
func remove_item(item:String) -> void:
	if item in inventory_list:
		inventory_list[item] -= 1
		if inventory_list[item] == 0:
			inventory_list.erase(item)

func return_items() -> Dictionary:
	return inventory_list 
	
#uses the list with relative chances of getting different items 
#to make an array and choose one item randomly
func generate_item() -> void:
	var probability_list = []
	for key in GlobalVars.possible_items.keys():
		for i in range(GlobalVars.possible_items[key][0]):
			probability_list.append(key)
	var number = GlobalVars.roll_dice(1, len(probability_list))-1
	add_item(probability_list[number])

func give_items() -> Array:
	var dropped_items = []
	for item_name in inventory_list:
		if item_name != "empty":
			for i in range(inventory_list[item_name]):
				dropped_items.append(item_name)
	return dropped_items
				
	

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
