extends Node2D

onready var map = get_parent()
var current_direction = 0
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	match_direction(current_direction)

#sets the stairs image to up or down
func match_direction(direction:int):
	current_direction = direction
	match direction:
		0:
			$Up.show()
			$Down.hide()
			
		1:
			$Up.hide()
			$Down.show()
			
#returns the direction of stairs, 0 - back, 1 - forward			
func get_direction() -> int:
	return current_direction 

#sets the position of stairs using tilemap coordinates
func set_map_pos(cell) -> void:
	set_position(map.map_to_world(cell))
	
#returns the position of stairs instance
func get_map_pos() -> Vector2:
	return map.world_to_map(get_position())	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
