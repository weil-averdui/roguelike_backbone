extends Node2D
var current_position: Vector2
onready var map = get_parent()
var flavor = "item_01"
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	set_visuals()

func set_visuals() -> void:
	var item_path
	if flavor in GlobalVars.possible_items:
		item_path = GlobalVars.possible_items[flavor][1]
		var item_scene = load(item_path).instance()
		for child in self.get_children():
			self.remove_child(child)
			child.queue_free()
		add_child(item_scene)
	else: 
		print("Error")
		
func place_item(cell: Vector2)-> void:
	current_position = cell
	set_position(map.map_to_world(cell))
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func set_flavor(item: String) -> void:
	flavor = item

func get_flavor() -> String:
	return flavor
