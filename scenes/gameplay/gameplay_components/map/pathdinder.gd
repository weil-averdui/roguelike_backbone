extends Node
onready var map = get_parent()
var aStar:AStar2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass 
	 # Replace with function body.

#picks all valid cells for pathfinding
func get_a_cells() -> void:
	aStar = AStar2D.new()
	aStar.reserve_space(map.a*map.b)
	for i in range(map.a):
		for j in range(map.b):
			if map.is_blocked(Vector2(i,j)) != "wall":
				var idx = getAStarCellId(Vector2(i,j))
				aStar.add_point(idx, Vector2(i,j))
	for i in range(map.a):
		for j in range(map.b):
			if map.is_blocked(Vector2(i,j)) != "wall":
				var idx = getAStarCellId(Vector2(i,j))
				for vNeighborCell in [Vector2(i,j-1),Vector2(i,j+1),Vector2(i-1,j),Vector2(i+1,j)]:
					var idxNeighbor=getAStarCellId(vNeighborCell)
					if aStar.has_point(idxNeighbor):
						aStar.connect_points(idx, idxNeighbor, false)



	
#gets AStar cell id based on the cell coordinates
func getAStarCellId(vCell:Vector2) -> int:
	var id = (vCell.y+vCell.x*map.b)
	return  id
	
#temporarily occupies a cell
func occupy(pos:Vector2) -> void:
	var idx = getAStarCellId(pos)
	if aStar.has_point(idx):
		aStar.set_point_disabled(idx, true)
		
#frees previously occupied cell
func freed(pos:Vector2) -> void:
	var idx = getAStarCellId(pos)
	if aStar.has_point(idx):
		aStar.set_point_disabled(idx, false)
		
#returns an array containing path between two points
func get_a_path(aaStar, start:Vector2, target:Vector2) -> Array:
	var idStart = getAStarCellId(start)
	var idTarget = getAStarCellId(target)
	if aaStar.has_point(idStart) and aaStar.has_point(idTarget):
		return Array(aaStar.get_point_path(idStart, idTarget))
	else:	
		return []
