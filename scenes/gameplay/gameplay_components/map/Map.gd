extends TileMap

var rng = GlobalVars.rng
onready var pathfinder = $pathdinder

export (PackedScene) var Actor
export (PackedScene) var Stairs
export (PackedScene) var Item
export var used_cells = []

onready var generator = get_node("generator")
var tile_map = self
var map_array = ["Ground"]
var entities = ["dummy"]
var hero_positions = ["start"]
var survivors = ["none"] 
var stairs_position = ["noescape"]
var stairs_array = ["defaultstair"]
var items = ["nothing"]
var item_position = ["treasures"]
#the saved floors should probably be turned into dictionaries 
#to make it easier to deal jumps over several unknown floors
var ids = [-1]
var item_ids = [-2]
var a = 20
var b = 20
var hero
export var hero_position: Vector2
var max_monsters = 10
var monsters_on_the_floor = 0
export var state = true
var game_over = false
var moves = ["right","left","up", "down", "right-up", "right-down", "left-up","left-down", "pick"]
var service = ["next", "previous"]
var hero_hp = 0
var current_floor = 1
var processing_state = true



#choses a free cell to place an item or object

func new_spawn() -> Vector2:
	var id = generator.rng.randi_range(0,len(used_cells)-1)
	return used_cells[id]
	
#Places an acting character of flavor type to location position
	
func spawn(flavor:String, location:Vector2) -> Object:
	var number
	if ids != [-1]:
		number = ids.max()+1
	else:
		ids.clear()
		entities.clear()
		number = 0
	var actor = Actor.instance()
	entities.append(actor)
	ids.append(number)
	add_child(actor)
	actor.set_character(flavor)
	actor.fighter.set_id(number)
	actor.put_actor(location)
	used_cells.erase(location)
	return actor
	
#places an item on map
func drop_item(flavor: String, location: Vector2) -> Object:
	var number
	if item_ids != [-2]:
		number = ids.max()+1
	else:
		item_ids.clear()
		items.clear()
		number = 0
	var item = Item.instance()
	items.append(item)
	item_ids.append(number)
	add_child(item)
	item.set_flavor(flavor)
	item.set_visuals()
	item.place_item(location)
	return item
	
#picks item and places it into inventory
func pick_item(cell: Vector2) -> void:
	var number = find_item(cell)
	if number>-1:
		var picked_item = items[number]
		hero.inventory.add_item(picked_item.get_flavor())
		picked_item.queue_free()
		items.remove(number)
		item_ids.remove(number)
		print(hero.inventory.return_items())
	
#returns an array representing a map of a layer - either an existing or new one
func go_to_level(level:int, width:int, height:int) -> Array:
	if level>=len(map_array):
		return new_level(width, height)
	else:
		return map_array[level]

#renders the tilemap
func render_map(map:Array) -> void:
	for i in len(map):
		for j in len(map[0]):
			tile_map.set_cell(j,i,map[i][j])
			
#generates a new floor array
func new_level(width, height) -> Array:
	var floor_map = generator.make_map(width, height)
	return floor_map
	
#returns if the tile is a wall, empty or occupied
func is_blocked(cell: Vector2) -> String:
	if get_cellv(cell)==1:
		return "wall"
	else:
		if entities != ["dummy"]:
			for actor in entities:
				if actor.current_position == cell:
					if actor.char_group == "foe":
						return "foe"
					else:
						return actor.char_type
		return "empty"
	
#generates second strairs with first one given
func one_stair_pos(loc1:Vector2 ) -> Array:
	var ids = range(0, len(used_cells))
	var id1 = used_cells.find(loc1)
	ids.remove(id1)
	var id2 = generator.rng.randi_range(0, len(ids)-1)
	var loc2 = used_cells[id2]

	return [loc1,loc2]

#add stairs to the scene
func add_stairs (positions: Array) -> void:
	var loc1 = positions[0]
	var loc2 = positions[1]
	var stairs1 = Stairs.instance() 
	stairs_array.append(stairs1)
	add_child(stairs1)
	stairs1.set_map_pos(loc1)
	stairs1.match_direction(0)
	var stairs2 = Stairs.instance() 
	stairs_array.append(stairs2)
	add_child(stairs2)
	stairs2.set_map_pos(loc2)
	stairs2.match_direction(1)

#adds stairs from an array with both locations and directions	
func add_existing_stairs(state) -> void:
	var loc1 = state[0][1]
	var loc2 = state[1][1]
	var dir1 = state[0][0]
	var dir2 = state[1][0]
	var stairs1 = Stairs.instance() 
	stairs1.match_direction(dir1)
	stairs_array.append(stairs1)
	add_child(stairs1)
	stairs1.set_map_pos(loc1)
	var stairs2 = Stairs.instance() 
	stairs2.match_direction(dir2)
	stairs_array.append(stairs2)
	add_child(stairs2)
	stairs2.set_map_pos(loc2)
	
#returns true if you want to go next level on next-level stairs or back on previous-level stairs
func check_stairs(direction:int, location: Vector2)-> bool:
	for i in range(1,len(stairs_array)):
		if (stairs_array[i] is  String) == false:
			if stairs_array[i].get_map_pos() == location and direction == stairs_array[i].get_direction():
				return true
	return false	
	
#sets a level using level and map size - both loading previous ones and generating new
func set_scene(level: int, width: int, height: int) -> void:
	var new_map = go_to_level(level, width, height)
	map_array.append(new_map)
	render_map(new_map)
	used_cells = tile_map.get_used_cells_by_id(0)
	tile_map.pathfinder.get_a_cells()
	if level >=len(hero_positions) or len(hero_positions)==1:
		hero_position = new_spawn()
	else:
		hero_position = hero_positions[level]
	if not hero:
		hero = spawn("hero", hero_position)
	else:
		hero.put_actor(hero_position)
		used_cells.erase(hero_position)
	monsters_on_the_floor = 0
	if level >=len(survivors):
		var monster_number = rng.randi_range(max_monsters/2, max_monsters+1)
		for i in range(0,monster_number):
			spawn("enemy_01", new_spawn())
			monsters_on_the_floor += 1
	else:
		for i in survivors[level]:
			spawn(i[2], i[1])
			entities[-1].fighter.set_hp(i[0])
	if level>=len(stairs_position):
		add_stairs(one_stair_pos(hero_position))
	else:
		add_existing_stairs(stairs_position[level]) 
	tile_map.pathfinder.get_a_cells()
	

func new_scene(width: int, height: int, current_level: int, next_level: int) -> void:
	var surviving_monsters = []
	for entity in entities:
		if entity.char_group == "foe":
			var position = entity.current_position
			var hp = entity.fighter.get_hp()
			var monster_type = entity.char_type
			var monster_state = [hp, position, monster_type]
			surviving_monsters.append(monster_state)
	if len(survivors)<=current_level:
		survivors.append(surviving_monsters)
	else:
		survivors[current_level] = []+surviving_monsters
	if len(hero_positions) <= current_level:
		hero_positions.append(hero_position)
	else:
		hero_positions[current_level] = hero_position
	while len(entities)>1:
		var last_entity = entities[-1]
		last_entity.queue_free()
		entities.pop_back()
		ids.pop_back()
	var stairs_on_floor =[]
	for i in range(1,3):
		var stair_position = stairs_array[i].get_map_pos()
		var stair_direction = stairs_array[i].get_direction()
		var stair_state = [stair_direction, stair_position]
		stairs_on_floor.append(stair_state)
	if len(stairs_position)<=current_level:
		stairs_position.append(stairs_on_floor)
	else:
		stairs_position[current_level] = []+stairs_on_floor
	while len(stairs_array)>1:
		var current_stair= stairs_array[-1]
		current_stair.queue_free()
		stairs_array.pop_back()	
	hero_hp = hero.fighter.get_hp()
	set_scene(next_level, width, height)
	current_floor = next_level
	hero.fighter.set_hp(hero_hp)
		
#checks if movement buttons were pressesd
func check_key(key_group:Array) -> bool:
	for key in key_group:
		if Input.is_action_just_pressed(key):
			return true
	return false
	

#function for processing input
func get_input() -> void:
	var move_vector = Vector2(0,0)
	if state and processing_state and !game_over:
		if check_key(moves):
			if Input.is_action_pressed("right"):
				 move_vector.x += 1
			if Input.is_action_pressed("left"):
				move_vector.x -= 1
			if Input.is_action_pressed("down"):
				move_vector.y += 1
			if Input.is_action_pressed("up"):
				move_vector.y -= 1
			if Input.is_action_pressed("right-up"):
				move_vector.x += 1
				move_vector.y -= 1
			if Input.is_action_pressed("right-down"):
				move_vector.x += 1
				move_vector.y += 1
			if Input.is_action_pressed("left-up"):
				move_vector.x -= 1
				move_vector.y -= 1
			if Input.is_action_pressed("left-down"):
				move_vector.x -= 1
				move_vector.y += 1
			if Input.is_action_pressed("pick"):
				pick_item(hero_position)
			processing_state = false
			hero.step(move_vector)
			yield(get_tree().create_timer(0.05), "timeout")
			for entity in entities:
				entity.do_the_move()
				hero_hp = hero.fighter.get_hp()
			processing_state = true
		elif check_key(service):
			if Input.is_action_just_pressed("previous") and check_stairs(0,hero.current_position):
				state = false
				go_back()
				yield(get_tree().create_timer(0.1), "timeout")
				state = true
			if Input.is_action_just_pressed("next") and check_stairs(1, hero.current_position):
				state = false
				go_next()
				yield(get_tree().create_timer(0.1), "timeout")
				state = true
func go_back() -> void:
	if current_floor > 1:
		new_scene(a, b, current_floor, current_floor-1)
	elif current_floor == 1:
		quit_dungeon()

func go_next() -> void:
	new_scene(a, b, current_floor, current_floor+1)

func quit_dungeon():
	pass
#finds acting character using its location
func find_entity(location:Vector2) -> int:
	for number in len(entities):
		if entities[number].current_position == location:
			return number
	return -1
	
func find_item(location:Vector2) -> int:
	for number in len(items):
		if (items[number] is String) == false:
			if items[number].current_position == location:
				return number
	return -1
		
#finds acting character using its ID	
func get_entity(id) -> int:
	for number in len(entities):
		if ids[number] == id:
			return number
	return -1

#reaction to the hit signal			
func _on_hit(attack_cell, damage_cell) -> void:
	var defendant_number = find_entity(damage_cell)
	var attacker_number = find_entity(attack_cell)
	if defendant_number>=0 and attacker_number>=0:
		var defendant = entities[defendant_number]
		var attacker = entities[attacker_number]
		var target_value=defendant.fighter.get_defense()
		var damage = attacker.fighter.do_attack(target_value)
		var dead = defendant.fighter.get_damage(damage[0])
		if dead >= 0:
			if defendant.char_type != "hero":
				var dropped_items = defendant.inventory.give_items()
				for item in dropped_items:
					drop_item(item, damage_cell)
				defendant.queue_free()
				entities.remove(defendant_number)
				ids.remove(defendant_number)
				used_cells.erase(damage_cell)
				monsters_on_the_floor -=1
				pathfinder.freed(damage_cell)
			else:
				print("game_over")
				game_over = true
		
	
# Called when the node enters the scene tree for the first time.
func _ready():
	set_scene(1,a,b)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _physics_process(delta):
	get_input()
	
