extends Node

var rng = RandomNumberGenerator.new()


#Map gen params
var a = 30
var b = 30
var max_size = 12
var min_size = 2
var max_num = 8
var min_num = 2
var hall_width = 1
var max_room_start = 16


#Character paths
var enemy_01_path = "res://scenes/gameplay/gameplay_components/actor/characters/Enemy_01.tscn"
var hero_path = "res://scenes/gameplay/gameplay_components/actor/characters/Hero.tscn"

#character stats
var hero_stats = {
	"max_hp":100,
	"attack_bonus": 5,
	"damage_bonus": 3,
	"damage_die": 6,
	"damage_number_dice": 1,
	"crit_rate":20,
	"crit_multiplier": 2,
	"detection_range": 6,
	"defense": 15,
	"speed":1,
}
var enemy_01_stats = {
	"max_hp":10,
	"attack_bonus": 4,
	"damage_bonus": 2,
	"damage_die": 4,
	"damage_number_dice": 1,
	"crit_rate":20,
	"crit_multiplier": 2,
	"detection_range": 6,
	"defense": 12,
	"speed": 1,
}

#path to sprite, stats dictionary, friendly: bool
var character_list = {
	"hero": [hero_path, hero_stats, true],
	"enemy_01":[enemy_01_path, enemy_01_stats, false]
}



var item_01_sprite = "res://scenes/gameplay/gameplay_components/object/items/item_01.tscn"

var possible_items = {
	"empty":[1, ""],
	"item_01": [1, item_01_sprite]
}

#dice roller
func roll_dice(number, dice):
	var res = 0
	for i in number:
		res+=rng.randi_range(1,dice)
	return res
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
